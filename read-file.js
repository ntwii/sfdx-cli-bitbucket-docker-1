const readline = require('readline');
const fs = require('fs');
const { exit } = require('process');

// create instance of readline
// each instance is associated with single input stream
let rl = readline.createInterface({
	input: fs.createReadStream(process.argv[2])
});

let complexComponentsArgv = process.argv[3] || false;

let allowedComponents = [
	'objects',
	'assignmentRules',
	'permissionsets',
	'applications',
	'autoResponseRules',
	'callCenters',
	'components',
	'connectedApps',
	'customMetadata',
	'duplicateRules',
	'escalationRules',
	'flowDefinitions',
	'flows',
	'homePageLayouts',
	'labels',
	'layouts',
	'nameCredentials',
	'objectTranslations',
	'queues',
	'quickActions',
	'remoteSiteSettings',
	'reportTypes',
	'roles',
	'settings',
	'sharingRules',
	'sites',
	'staticresources',
	'surveySettings',
	'tabs',
	'workflows',
	'contentassets',
	'flexipages',
	'globalValueSets',
	'groups',
	'translations',
	'profiles',
	'classes',
	'pages',
	'triggers',
	'aura',
	'lwc',
	'email',
	'matchingRules',
	'standardValueSets'
];

if(complexComponentsArgv !== false && typeof complexComponentsArgv === 'string' && complexComponentsArgv.length > 0) {
	let complexArr = complexComponentsArgv.split(',');

	allowedComponents = allowedComponents.filter(function(value, index, arr){
		return complexArr.indexOf(value) === -1;
	});
}

let object_count = {};
let filesToDeployList = [];
let filesToDeleteList = [];

// event is emitted after each line
rl.on('line', function (line) {
	if (line.startsWith('M') || line.startsWith('A') || line.startsWith('D')) {
		let t = line.split('\t');
		if (t.length > 0) {
			let status = t[0];
			let file_path = t[1];

			if (status == 'M' || status == 'A') {
				if (file_path.startsWith('force-app/main/default/standardValueSets/') ||
					file_path.startsWith('force-app/main/default/objects/') ||
					file_path.startsWith('force-app/main/default/assignmentRules/') ||
					file_path.startsWith('force-app/main/default/permissionsets/') ||
					file_path.startsWith('force-app/main/default/applications/') ||
					file_path.startsWith('force-app/main/default/autoResponseRules/') ||
					file_path.startsWith('force-app/main/default/callCenters/') ||
					file_path.startsWith('force-app/main/default/components/') ||
					file_path.startsWith('force-app/main/default/connectedApps/') ||
					file_path.startsWith('force-app/main/default/customMetadata/') ||
					file_path.startsWith('force-app/main/default/duplicateRules/') ||
					file_path.startsWith('force-app/main/default/escalationRules/') ||
					file_path.startsWith('force-app/main/default/flowDefinitions/') ||
					file_path.startsWith('force-app/main/default/flows/') ||
					file_path.startsWith('force-app/main/default/homePageLayouts/') ||
					file_path.startsWith('force-app/main/default/labels/') ||
					file_path.startsWith('force-app/main/default/layouts/') ||
					file_path.startsWith('force-app/main/default/nameCredentials/') ||
					file_path.startsWith('force-app/main/default/objectTranslations/') ||
					file_path.startsWith('force-app/main/default/queues/') ||
					file_path.startsWith('force-app/main/default/quickActions/') ||
					file_path.startsWith('force-app/main/default/remoteSiteSettings/') ||
					file_path.startsWith('force-app/main/default/reportTypes/') ||
					file_path.startsWith('force-app/main/default/roles/') ||
					file_path.startsWith('force-app/main/default/settings/') ||
					file_path.startsWith('force-app/main/default/sharingRules/') ||
					file_path.startsWith('force-app/main/default/sites/') ||
					file_path.startsWith('force-app/main/default/staticresources/') ||
					file_path.startsWith('force-app/main/default/surveySettings/') ||
					file_path.startsWith('force-app/main/default/tabs/') ||
					file_path.startsWith('force-app/main/default/workflows/') ||
					file_path.startsWith('force-app/main/default/contentassets/') ||
					file_path.startsWith('force-app/main/default/flexipages/') ||
					file_path.startsWith('force-app/main/default/globalValueSets/') ||
					file_path.startsWith('force-app/main/default/groups/') ||
					file_path.startsWith('force-app/main/default/translations/') ||
					file_path.startsWith('force-app/main/default/email/') ||
					file_path.startsWith('force-app/main/default/matchingRules/') ||
					file_path.startsWith('force-app/main/default/profiles/')
				) {
					let tmp = file_path.replace('force-app/main/default/', '');
					let arr = tmp.split('/');
					// console.log('[' + file_path + '] ' + tmp + " = " + arr.join("|") + '\n');

					if (arr.length > 1) {
						if (!(arr[1] in object_count)) {
							object_count[arr[1]] = 0;
						}else{
							object_count[arr[1]]++;
						}

						if(allowedComponents.indexOf(arr[0]) !== -1 && filesToDeployList.indexOf('force-app/main/default/' + arr[0] + '/' + arr[1]) == -1){
							filesToDeployList.push('force-app/main/default/' + arr[0] + '/' + arr[1]);
						}
					}
				} else if (file_path == 'manifest/package.xml') {

				} else if (file_path.startsWith('force-app/main/default/classes/') ||
					file_path.startsWith('force-app/main/default/pages/') ||
					file_path.startsWith('force-app/main/default/triggers/')
				) {
					let tmp = file_path.replace('force-app/main/default/', '');
					let arr = tmp.split('/');

					if (arr.length > 1 && allowedComponents.indexOf(arr[0]) !== -1 && !file_path.endsWith('-meta.xml') && filesToDeployList.indexOf(file_path) == -1) {
						filesToDeployList.push(file_path);
					}
				} else if (file_path.startsWith('force-app/main/default/aura/') ||
					file_path.startsWith('force-app/main/default/lwc/')) {
					let tmp = file_path.replace('force-app/main/default/', '');
					let arr = tmp.split('/');
					if (arr.length > 1) {
						let component_name = 'force-app/main/default/' + arr[0] + '/' + arr[1];
						if (allowedComponents.indexOf(arr[0]) !== -1 && filesToDeployList.indexOf(component_name) == -1) {
							filesToDeployList.push(component_name);
						}
					}
				} else {

				}
			} else if (status == 'D') {
				if (file_path.startsWith('force-app/main/default/classes/') ||
					file_path.startsWith('force-app/main/default/pages/') ||
					file_path.startsWith('force-app/main/default/triggers/')
				) {
					if (!file_path.endsWith('-meta.xml') && filesToDeployList.indexOf(file_path) == -1) {
						filesToDeleteList.push(file_path);
					}
				}
			}
		}
	}
});

// end
rl.on('close', function (line) {
	if (filesToDeployList.length > 0) {
		fs.writeFileSync('changes.txt', filesToDeployList.join(','));
	}

	if (filesToDeleteList.length > 0) {
		fs.writeFileSync('deletes.txt', filesToDeleteList.join(','));
	}
});